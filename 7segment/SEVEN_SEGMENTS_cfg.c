/*
 * File: SEVEN_SEGMENTS_cfg.c
 * Driver Name: [[ 7-Segments Display ]]
 * SW Layer:   ECUAL
 * Created on: Jun 28, 2020
 * Author:     Khaled Magdy
 * -------------------------------------------
 * For More Information, Tutorials, etc.
 * Visit Website: www.DeepBlueMbedded.com
 *
 */

#include "SEVEN_SEGMENTS.h"
#include "main.h"

const SEVEN_SEG_CfgType SEVEN_SEG_CfgParam[SEVEN_SEG_UNITS] =
{
	// 7-Segments Display Unit 1 Configurations
    {
    	/* Seven Segments Pins Info */
	    {GPIOB, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB},
		{a_Pin, b_Pin, c_Pin, d_Pin, e_Pin, f_Pin, g_Pin},
		/* Enable Control Signal's Pins */
		{GPIOA, GPIOA, GPIOA},
		{Q1_Pin, Q2_Pin, Q3_Pin},
	    /* Enable Digit Dot */
	    {GPIOA},
	    {DP_Pin}
	}
};
