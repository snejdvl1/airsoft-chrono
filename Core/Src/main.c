/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "../../7segment/SEVEN_SEGMENTS.h"
#include "../../flash/FLASH_PAGE.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TIME_CONSTANT 6.25E-6// prescaler+1/timer base clock  (=200/32 000 000)
#define DISTANCE 0.05
#define MSTOFPS 3.28084

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim14;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint8_t Meas_init = 1;
int counter = 0;
float Time = 0;
float speed =0;


uint8_t measure_flag = 0;
uint8_t speed_format = 0;

int weight = 25;

int rof_counter = 0;




uint32_t FirstSector = 0, NbOfSectors = 0, Address = 0;
uint32_t SectorError = 0;
__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

/*Variable used for Erase procedure*/
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM14_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM14_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  // Setup SysTick Timer for 1ms interrupts
  SEVEN_SEG_Init(0);
  SEVEN_SEG_Enable(0);
  SEVEN_SEG_Write(0, 404);

  HAL_TIM_Base_Start_IT(&htim14);
  HAL_Delay(850);

  weight = Flash_Read_NUM(0x800D000);
  if(( weight > 100 || weight < 0)){
	  weight = 25;
  }
  speed_format = Flash_Read_NUM(0x800D800);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  //HAL_UART_Transmit(&huart2, "hello\r\n\n", 8, 100);

while(1){
  while (HAL_GPIO_ReadPin(GPIOB, MEASUREMODE_Pin) == 0)
  {
	  if(measure_flag == 1){
	  	//char aTxBuffer[16];
	  	for(int i = 0; i<10; i++){
			HAL_Delay(100);
			if(measure_flag == 2){
				//sprintf(aTxBuffer, "%f\r\n", Time);
				//HAL_UART_Transmit(&huart2, (uint8_t *)aTxBuffer, strlen(aTxBuffer), 100);
				speed = (DISTANCE/Time);
				switch(speed_format) {
				   case 0  :
					   //sprintf(aTxBuffer, "MPS %d\r\n", (int)speed);
					   SEVEN_SEG_Write(0, (int)speed);
				      break; /* optional */

				   case 1  :
					   speed = speed * MSTOFPS;
					   //sprintf(aTxBuffer, "FPS %d\r\n", (int)speed);
					   SEVEN_SEG_Write(0, (int)speed);
				      break; /* optional */

				   case 2  :
					   speed = 0.5 * weight * 0.00001 * speed*speed;
					   //sprintf(aTxBuffer, "J %d\r\n", (int)speed);
					   SEVEN_SEG_Write(0, (int)speed);
				      break; /* optional */

				   default :
					   //sprintf(aTxBuffer, "MPS %d\r\n", (int)speed);
					   SEVEN_SEG_Write(0, (int)speed);
				}
				break;
			}
	  	}
	  	if(measure_flag == 1){
			//sprintf(aTxBuffer, "Err\r\n");
			SEVEN_SEG_Err(0);
			HAL_TIM_Base_Stop_IT(&htim1);
	  	}
	  	measure_flag = 0;
	  	//HAL_UART_Transmit(&huart2, (uint8_t *)aTxBuffer, strlen(aTxBuffer), 100);

	  } else{
		  if(HAL_GPIO_ReadPin(GPIOA, MODE_Pin) == 1){
			  while(1){
				  HAL_Delay(1);
				  if(HAL_GPIO_ReadPin(GPIOA, MODE_Pin)==0){
					  speed_format++;
					  if(speed_format >= 3){
						  speed_format = 0;
					  }


					  switch(speed_format) {
						   case 0  :
							  // HAL_UART_Transmit(&huart2, "Meters per second mode\r\n", 24, 100);
							   SEVEN_SEG_MPS(0);
							   Flash_Write_NUM(0x800D800, speed_format);
							  break; /* optional */

						   case 1  :
							  // HAL_UART_Transmit(&huart2, "Feet per second mode\r\n", 22, 100);
							   SEVEN_SEG_FPS(0);
							   Flash_Write_NUM(0x800D800, speed_format);
							  break; /* optional */

						   case 2  :
							  // HAL_UART_Transmit(&huart2, "Joules mode\r\n", 13, 100);
							   SEVEN_SEG_J(0);
							   Flash_Write_NUM(0x800D800, speed_format);
							  break; /* optional */

						   default :
							  // HAL_UART_Transmit(&huart2, "Meters per second mode\r\n", 24, 100);
							   SEVEN_SEG_MPS(0);
						}


					  break;
				  }
			  }
		  }
		  if(Meas_init == 1){
			  switch(speed_format) {
				   case 0  :
					   SEVEN_SEG_MPS(0);
					  break; /* optional */

				   case 1  :
					   SEVEN_SEG_FPS(0);
					  break; /* optional */

				   case 2  :
					   SEVEN_SEG_J(0);
					  break; /* optional */

				   default :
					   SEVEN_SEG_MPS(0);
				}
			  Meas_init = 0;
			  Flash_Write_NUM(0x800D000, weight);

		  }
	  }
  }

  while (HAL_GPIO_ReadPin(GPIOB, ROFMODE_Pin) == 0)
  {
	  Meas_init = 1;
	  while(rof_counter > 0){
		  int rof_now = rof_counter;
		  HAL_Delay(500);
		  if(rof_now == rof_counter){
			 // char aTxBuffer[16];
			  //sprintf(aTxBuffer, "ROF %d\r\n", (int)rof_counter);
			 // HAL_UART_Transmit(&huart2, (uint8_t *)aTxBuffer, strlen(aTxBuffer), 100);
			  rof_counter = 0;
			  break;
		  }
	  }
  }

  while (HAL_GPIO_ReadPin(GPIOB, WEIGHTMODE_Pin) == 0)
  {
	  Meas_init = 1;
	 // char aTxBuffer[16];
	 // sprintf(aTxBuffer, "\r0.%d", weight);
	 // HAL_UART_Transmit(&huart2, (uint8_t *)aTxBuffer, strlen(aTxBuffer), 100);
	  SEVEN_SEG_Write_W(0, (int)weight);
	  HAL_Delay(50);

	  if(HAL_GPIO_ReadPin(GPIOA, UP_Pin)==1){
		  while(1){
			  HAL_Delay(1);
			  if(HAL_GPIO_ReadPin(GPIOA, UP_Pin)==0){
				weight++;
				  break;
			  }
		  }
	  }
	  if(HAL_GPIO_ReadPin(GPIOA, DOWN_Pin)==1){
		  while(1){
			  HAL_Delay(1);
			  if(HAL_GPIO_ReadPin(GPIOA, DOWN_Pin)==0){
				weight--;
				  break;
			  }
		  }
	  }
	  if(HAL_GPIO_ReadPin(GPIOA, MODE_Pin)==1){
		  while(1){
			  HAL_Delay(1);
			  if(HAL_GPIO_ReadPin(GPIOA, MODE_Pin)==0){
				  if(weight==25){
				  weight = 32;}
				  else if(weight==32){
					  weight = 35;}
				  else if(weight==35){
					  weight = 40;}
				  else if(weight==40){
					  weight = 25;}
				  else{
				      weight = 25;}
				  break;
			  }
		  }
	  }
  }
}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* EXTI0_1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 199;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 199;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 4294967295;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 0;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 19999;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, DP_Pin|Q1_Pin|Q2_Pin|Q3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, a_Pin|b_Pin|c_Pin|d_Pin
                          |e_Pin|f_Pin|g_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : WEIGHTMODE_Pin MEASUREMODE_Pin ROFMODE_Pin */
  GPIO_InitStruct.Pin = WEIGHTMODE_Pin|MEASUREMODE_Pin|ROFMODE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : GATE1_Pin GATE0_Pin */
  GPIO_InitStruct.Pin = GATE1_Pin|GATE0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : MODE_Pin UP_Pin DOWN_Pin */
  GPIO_InitStruct.Pin = MODE_Pin|UP_Pin|DOWN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : DP_Pin Q1_Pin Q2_Pin Q3_Pin */
  GPIO_InitStruct.Pin = DP_Pin|Q1_Pin|Q2_Pin|Q3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : a_Pin b_Pin c_Pin d_Pin
                           e_Pin f_Pin g_Pin */
  GPIO_InitStruct.Pin = a_Pin|b_Pin|c_Pin|d_Pin
                          |e_Pin|f_Pin|g_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	// preruseni pin 0
	if(GPIO_Pin == GATE0_Pin){
		TIM1->CNT = 0;
		HAL_TIM_Base_Start_IT(&htim1);
		rof_counter++;

		measure_flag = 1;
	}

	// preruseni pin 1
	else if(GPIO_Pin == GATE1_Pin){
		HAL_TIM_Base_Stop_IT(&htim1);
		counter = TIM1->CNT;
		Time = TIME_CONSTANT*counter;

		measure_flag = 2;
	}

	// buhvico
	else{
		__NOP();
	}
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
