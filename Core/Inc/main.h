/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void Seg_Tim_CallBack(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define WEIGHTMODE_Pin GPIO_PIN_9
#define WEIGHTMODE_GPIO_Port GPIOB
#define GATE1_Pin GPIO_PIN_0
#define GATE1_GPIO_Port GPIOA
#define GATE1_EXTI_IRQn EXTI0_1_IRQn
#define GATE0_Pin GPIO_PIN_1
#define GATE0_GPIO_Port GPIOA
#define GATE0_EXTI_IRQn EXTI0_1_IRQn
#define T_VCP_TX_Pin GPIO_PIN_2
#define T_VCP_TX_GPIO_Port GPIOA
#define T_VCP_RX_Pin GPIO_PIN_3
#define T_VCP_RX_GPIO_Port GPIOA
#define MODE_Pin GPIO_PIN_4
#define MODE_GPIO_Port GPIOA
#define UP_Pin GPIO_PIN_5
#define UP_GPIO_Port GPIOA
#define DP_Pin GPIO_PIN_6
#define DP_GPIO_Port GPIOA
#define a_Pin GPIO_PIN_0
#define a_GPIO_Port GPIOB
#define b_Pin GPIO_PIN_1
#define b_GPIO_Port GPIOB
#define c_Pin GPIO_PIN_2
#define c_GPIO_Port GPIOB
#define Q1_Pin GPIO_PIN_8
#define Q1_GPIO_Port GPIOA
#define Q2_Pin GPIO_PIN_9
#define Q2_GPIO_Port GPIOA
#define Q3_Pin GPIO_PIN_10
#define Q3_GPIO_Port GPIOA
#define DOWN_Pin GPIO_PIN_12
#define DOWN_GPIO_Port GPIOA
#define d_Pin GPIO_PIN_3
#define d_GPIO_Port GPIOB
#define MEASUREMODE_Pin GPIO_PIN_4
#define MEASUREMODE_GPIO_Port GPIOB
#define ROFMODE_Pin GPIO_PIN_5
#define ROFMODE_GPIO_Port GPIOB
#define e_Pin GPIO_PIN_6
#define e_GPIO_Port GPIOB
#define f_Pin GPIO_PIN_7
#define f_GPIO_Port GPIOB
#define g_Pin GPIO_PIN_8
#define g_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
