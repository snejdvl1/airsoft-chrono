################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../flash/FlASH_PAGE.c 

OBJS += \
./flash/FlASH_PAGE.o 

C_DEPS += \
./flash/FlASH_PAGE.d 


# Each subdirectory must supply rules for building sources it contributes
flash/%.o flash/%.su: ../flash/%.c flash/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -DUSE_HAL_DRIVER -DSTM32G031xx -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/65vla/OneDrive/Documents/LAR/chrono/flash" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-flash

clean-flash:
	-$(RM) ./flash/FlASH_PAGE.d ./flash/FlASH_PAGE.o ./flash/FlASH_PAGE.su

.PHONY: clean-flash

