################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../7segment/SEVEN_SEGMENTS.c \
../7segment/SEVEN_SEGMENTS_cfg.c 

OBJS += \
./7segment/SEVEN_SEGMENTS.o \
./7segment/SEVEN_SEGMENTS_cfg.o 

C_DEPS += \
./7segment/SEVEN_SEGMENTS.d \
./7segment/SEVEN_SEGMENTS_cfg.d 


# Each subdirectory must supply rules for building sources it contributes
7segment/%.o 7segment/%.su: ../7segment/%.c 7segment/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G031xx -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/bruh/OneDrive/Documents/LAR/chrono/flash" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@"  -mfloat-abi=soft -mthumb -o "$@"

clean: clean-7segment

clean-7segment:
	-$(RM) ./7segment/SEVEN_SEGMENTS.d ./7segment/SEVEN_SEGMENTS.o ./7segment/SEVEN_SEGMENTS.su ./7segment/SEVEN_SEGMENTS_cfg.d ./7segment/SEVEN_SEGMENTS_cfg.o ./7segment/SEVEN_SEGMENTS_cfg.su

.PHONY: clean-7segment

